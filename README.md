# Microservices Keycloak Präsentation

Für meine Keycloak Präsentation habe ich ein kleines Beispiel Angular Projekt entwickelt, damit Ihr wisst, wie ihr es bei euren Anwendungen nutzen könnt. Das Beispiel ist sehr simpel gehalten und soll auch nur die Anbindung an Keycloak zeigen.

![Bild](./img/image.png)


## Inhalte

*  [Getting Started](#getting-started)
    *   [Vorraussetzungen](#vorraussetzungen)
*  [Repo klonen](#repo-klonen)
*  [Projekt installieren](#projekt-installieren)
*  [Docker Container starten](#docker-container-starten)
*  [Keycloak Adminseite](#keycloak-adminseite)


## Getting Started

Damit ihr das Programm nutzen könnt, müsst ihr Node.js und Docker mit Docker-Compose (vorzugsweise Docker Desktop) installiert haben.

### Vorraussetzungen
- [Node.js](https://nodejs.org/en/download)
- [Docker Desktop](https://www.docker.com/products/docker-desktop/)

## Repo klonen

Nun öffnet ihr das Terminal oder Git Bash und gebt folgenden Befehl ein, um das Repo zu klonen:



```
git clone https://git.thm.de/nlng17/microservices-praesentation.git
```

## Projekt installieren

Navigiert anschließend in den Ordner:

```
cd microservices-praesentation
cd myapp
```

Und installiert die benötigten Npm Pakete:

```
npm install
```



## Docker Container starten
Startet nun die Docker-Container:

```
docker-compose up -d
```

Es sollten der Postgres und der Keycloak Container starten

![Docker Container](./img/image-4.png)

![Keycloak Container](./img/image-1.png)

Es kann eine Weile dauern bis der Keycloak Container bereit ist.

## Keycloak Adminseite

Öffnet Keycloak in eurem Webbrowser.

http://localhost:8080


![image-2.png](./img/image-2.png)

Klickt dort auf "Administration Console".

Melden Sie sich dort an. 

- Benutzername: admin
- Passwort: admin

![image-5.png](./img/image-5.png)

Hier könnt ihr die Keycloak konfiguration sehen. Die Konfigurationsdaten wurden aus der init/thm-realm.json Datei geladen.

## Anwendung starten

Wenn ihr noch mit dem Terminal/Git-Bash in dem Ordner myapp seid dann könnt ihr die Anwendung starten:

```
npm start
```

Ruft die Seite http://localhost:4200 auf.

Wenn alles funktioniert hat werden Ihr direkt an den Keycloak Login weitergeleitet.

![image-6.png](./img/image-6.png)

Dort könnt ihr euch mit den Testzugangsdaten anmelden:

- Benutzername: simpleuser
- Passwort: test

Ihr solltet jetzt wieder auf die Startseite der Anwendung weitergeleitet werden. Dort solltet ihr nun wie auf dem ersten Bild die Profildaten des simpleusers sehen.


