export const environment = {
    keycloakurl: 'http://localhost:8080/auth',
    realm: 'THM',
    clientId: 'thmfind',
    redirecturi: 'http://localhost:4200'
};
