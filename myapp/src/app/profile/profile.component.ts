import {Component, OnInit} from '@angular/core';
import { AuthGuard } from '../guard/auth.guard';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent{
  
  public email?: String;
  public firstname? : String;
  public lastname? : String;
  public username? : String;
  public roles? : String[];

  constructor(private authGuard : AuthGuard) {
    authGuard.getProfile().then(profile => {
      this.email = profile.email;
      this.firstname = profile.firstName;
      this.lastname = profile.lastName;
      this.username = profile.username;
    })

    this.roles = authGuard.getRoles();
  }

}
