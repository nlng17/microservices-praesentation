import { KeycloakService } from "keycloak-angular";
import { environment } from "src/environments/environment.development";

export function initializeKeycloak(
  keycloak: KeycloakService
  ) {
    return () =>
      keycloak.init({
        config: {
          url: environment.keycloakurl,
          realm: environment.realm,
          clientId: environment.clientId,
        },
        initOptions: {
          pkceMethod: 'S256', 
          redirectUri: environment.redirecturi,
          checkLoginIframe: false
        }
      });
}