import { Component, OnInit } from '@angular/core';
import { KeycloakAuthGuard } from 'keycloak-angular';
import { AuthGuard } from '../guard/auth.guard';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent implements OnInit{
  public username : String = "";
  public roles : String[] = [];

  constructor(private authGuard: AuthGuard) {
  }
  ngOnInit(): void {
    this.roles = this.authGuard.getRoles();

    console.log(this.roles);
  }
  onLogout(event: Event) {
    this.authGuard.Logout();
  }
}
